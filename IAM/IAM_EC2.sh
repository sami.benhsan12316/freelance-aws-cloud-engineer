#!/bin/bash

# Set variables
ROLE_NAME="LambdaS3AccessRole"
POLICY_NAME="LambdaS3AccessPolicy"
S3_BUCKET_NAME="your-s3-bucket-name"
LAMBDA_FUNCTION_NAME="LambdaS3Function"
LAMBDA_RUNTIME="python3.8"
LAMBDA_HANDLER="lambda_function.lambda_handler"
LAMBDA_ZIP="lambda_function.zip"

# 1/ Create the IAM Policy (Save JSON to a file)
cat > policy.json <<EOL
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "s3:GetObject",
        "s3:PutObject",
        "s3:ListBucket"
      ],
      "Resource": [
        "arn:aws:s3:::$S3_BUCKET_NAME",
        "arn:aws:s3:::$S3_BUCKET_NAME/*"
      ]
    }
  ]
}
EOL

aws iam create-policy --policy-name $POLICY_NAME --policy-document file://policy.json

# 2/ Create IAM Role for Lambda (Trust Policy)
cat > trust-policy.json <<EOL
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": { "Service": "lambda.amazonaws.com" },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOL

aws iam create-role --role-name $ROLE_NAME --assume-role-policy-document file://trust-policy.json

# 3/ Attach Policy to Role
POLICY_ARN=$(aws iam list-policies --query "Policies[?PolicyName=='$POLICY_NAME'].Arn" --output text)
aws iam attach-role-policy --role-name $ROLE_NAME --policy-arn $POLICY_ARN

# Attach AWS Lambda Basic Execution Role for logging
aws iam attach-role-policy --role-name $ROLE_NAME --policy-arn arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole

# Wait for IAM role propagation
sleep 10

# 4/ Create Lambda Function (Requires a ZIP File)
# Ensure you have a lambda_function.py and zip it before running the script
if [ ! -f "$LAMBDA_ZIP" ]; then
    echo "⚠️ Lambda deployment package ($LAMBDA_ZIP) not found. Please create it before running this script."
    exit 1
fi

ROLE_ARN=$(aws iam get-role --role-name $ROLE_NAME --query "Role.Arn" --output text)

aws lambda create-function --function-name $LAMBDA_FUNCTION_NAME \
    --runtime $LAMBDA_RUNTIME \
    --role $ROLE_ARN \
    --handler $LAMBDA_HANDLER \
    --zip-file fileb://$LAMBDA_ZIP

echo "✅ Lambda Function '$LAMBDA_FUNCTION_NAME' created successfully with IAM Role '$ROLE_NAME'!"
