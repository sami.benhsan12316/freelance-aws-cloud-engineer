Commands:
RQ1 : Les fonction sous la forme mot1-mot2-....-motn 
RQ2 : Les attributs sont ss le format --mot1-mot2 
1/ run-instances: 
    Syntaxe: aws ec2 run-instances --image-id ami-xxxxxxxx --instance-type t2.micro --key-name YourKeyPair \ 
    --iam-instance-profile Name=MyInstanceProfile 
    Role : Tesna3lik Mékina EC2 bl image w type w clé ssh li inti talébhom 
    NB: Makina juste simulation ba3d mkch bch téjm taccédilha 
    # Q: --iam-instance-profile Name=MyInstanceProfile ???? 

2/  describe-instances: 
    Syntaxe: aws ec2 describe-instances --query 'Reservations[*].Instances[*].[InstanceId,InstanceType,State.Name]'
    Role: Tlistilk les ec2 dispo fl AWS mte3k .. 
    RQ:query héki bch tfaltark chnouma les champs li thb tchoufhom ml output Json li bch yjik  
    PB : Kiféh nextracti juste valeur ta3 champs (id par exp .. ) 
    SOL: --output text 
    # Plus généralement describe-resource bch tlisti les resources dispos .. 

3/ terminate-instances: 
    Syntaxe: aws ec2 terminate-instances --instance-ids i-xxxxxxxxxxxxxx
    Role: Tsakar mékina d'id i-xxxxxxxxxxxxxx
    PB: Lansitha w mahabtch tsakar .. à voir ba3d nchalah (yédha .. pas un pb majeur .. 
    khatér normalement c'est liée à LocalStack .. )

4/ create-security-group: 
    Syntaxe: aws ec2 create-security-group --group-name MySecurityGroup --description "My security group"
    Role: Tesna3lik SG avec le nom MySecurityGroup

5/ authorize-security-group-ingress :
    Syntaxe: aws ec2 authorize-security-group-ingress --group-id sg-xxxxxxxx --protocol tcp --port 22 --cidr 0.0.0.0/0
    Role: Bch tconfiguri el ingress rules ll SG hétha 

6/ authorize-security-group-egress : # A faire nchalah .. hia moujouda w makharajhéch ..
   aws ec2 authorize-security-group-egress --port all --protocol -1 --cidr 0.0.0.0/0  
   # all : tt les port 
   # -1 : tt les protocol 

7/ describe-key-pairs : 
    Syntaxe: aws ec2 describe-key-pairs --query 'KeyPairs[*].KeyName'
    Role: Tlistilk les key-pairs dispo 

8/ allocate-adress: 
    Syntaxe: aws ec2 allocate-address --domain vpc
    Role: Tallouwilk EIP (Elastic @IP)
    # Q: --domain: vpc .. chma3néha ? 
      R: domain héki no9sdou biha win bch nesta3ml el @EIP .. dans notre cas dans un vpc 
      possiblité okhra énk ta3ml standard .. ma3néha dans une EC2 

9/ associate-address:
   Syntaxe: aws ec2 associate-address --instance-id i-xxxxxxxxxxxxxx --allocation-id eip-xxxxxxxx
   Role: Naffécti EIP d'id ..  ll instance d'id .... 


10/ create-volume: 
    Syntax: aws ec2 create-volume --availability-zone us-east-1a --size 20 --volume-type gp2
    Role: Tesna3lik Amazon EBS volume 
    RQ: size bl GB , gp2 héki ma3néha ssd ta9rib .. w hano ba3d fl values ghad ..
    # Q: Fama kén EBS volume ekhi ? 


11/ attach-volume :
    Syntaxe: aws ec2 attach-volume --volume-id vol-xxxxxxxx --instance-id i-xxxxxxxxxxxxxx --device /dev/sdf
    Role: Tattachi volume l mékinét EC2 
    # Q: device : --device: The device name to expose the volume on the instance.

################## IAM COMMANDS FOR EC2 SERVICE ######################### 


